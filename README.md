## Time-Tracker
Time-Tracker keeps track of what you are working on and for how long.
Update JIRA with all you work at the end of the day or as often as you like during the day.
Time-Tracker supports [Pass: The Standard Unix Password Manager](https://www.passwordstore.org).
Just set the passord in the config  file to `pass:<path to store item>` and Time-Tracker will use that.

| Keybinding| Command|
--- | ---
CTRL+F10 | Update JIRA|


## TODO:

* Change stop button to pause button - start again where paused.
When paused button text should change to stop - and pressing the button
again should stop the tracker. The current active ticket label should be
moved to the input text field, which at the same time should be set to
read only until task is started again or stopped. The label should then
be set to "PAUSED". Duration on paused trackers must be accumulated to
one record.

* Load predefined buttons with names and tickets from file
