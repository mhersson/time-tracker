import QtQuick 2.13
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.11
import CustomQmlTypes 1.0

ItemTemplate {
    id: root

	Material.theme: Material.Dark
	Material.accent: Material.DeepPurple
    focus: true
    Keys.onPressed: {
        if ((event.key == Qt.Key_F10) && (event.modifiers & Qt.ControlModifier)) {
            root.updateJIRA();
        }
    }

    activityLabel: ObjectTemplate{}
    startButton: ObjectTemplate{text: "start"}

    Pane {
        id: pane
        anchors.fill: parent

    }

	ColumnLayout {
        anchors.fill: parent
        spacing: 2

        Row {
            Layout.alignment: Qt.AlignCenter
            spacing: 15

            Label {
                id: lbl_tracking
                width: 60
                height: 15
                text: qsTr("Tracking:")
                y: 10
            }

            Label {
                id: lbl_active_ticket
                width: 125
                height: 15
                text: activityLabel.text
                y: 10
            }

            TextField {
                id: tf_ticket
                width: 155
                placeholderText: "Enter ticket #"
                Keys.onReturnPressed:  {
                    root.startTracker(tf_ticket.text);
                    tf_ticket.clear()
                }
            }
        }

        Row {
            spacing: 30
            Layout.alignment: Qt.AlignCenter

            RoundButton {
                id: btn_start
                width: 160
                height: 50
                text: startButton.text
                Material.foreground: "lightgreen"
                onClicked:  {
                    root.startTracker(tf_ticket.text);
                    tf_ticket.clear()
                }
            }

            RoundButton {
                id: btn_stop
                width: 160
                height: 50
                text: "Stop"
                Material.foreground: "red"
                onClicked: root.stopTracker(lbl_active_ticket.text);
            }
        }

        Row {
            spacing: 3
            Layout.alignment: Qt.AlignCenter

            RoundButton {
                id: btn_meeting
                text: "meeting"
                Material.foreground: "lightblue"
                onClicked: root.startTracker("meeting");
            }

            RoundButton {
                id: btn_pserv
                text: "pserv-ass"
                Material.foreground: "lightblue"
                onClicked: root.startTracker("pserv assistance");
            }

            RoundButton {
                id: btn_sysmain
                text: "sys-maint"
                Material.foreground: "lightblue"
                onClicked: root.startTracker("sys maintenance");
            }

            RoundButton {
                id: btn_training
                text: "Training"
                Material.foreground: "lightblue"
                onClicked: root.startTracker("training");
            }
        }
	}
}
