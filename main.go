package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/quick"
	"github.com/therecipe/qt/quickcontrols2"
	"github.com/therecipe/qt/widgets"
	"gopkg.in/yaml.v2"
)

var tracker = timeTracker{}
var activityTicketName = map[string]string{"meeting": "devtr-1",
	"pserv assistance": "devtr-3",
	"sys maintenance":  "devtr-5",
	"training":         "devtr-8"}
var activities []activity
var ttlogdir string
var cfg config

func init() {
	ObjectTemplate_QmlRegisterType2("CustomQmlTypes", 1, 0, "ObjectTemplate")
	ItemTemplate_QmlRegisterType2("CustomQmlTypes", 1, 0, "ItemTemplate")

	home, err := homedir.Dir()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	ttlogdir = path.Join(home, ".config/time-tracker/logs")

	// If $HOME/.config/time-tracker/logs does not exist, create it
	if _, err := os.Stat(ttlogdir); os.IsNotExist(err) {
		os.MkdirAll(ttlogdir, 0755)
	}

	loadConfig(home)

	filename := path.Join(ttlogdir,
		"timetracker_"+time.Now().Format("02.Jan.2006")+".yaml")
	if _, err := os.Stat(filename); err == nil {
		loadActivitiesFromFile(filename)
	}
}

//ItemTemplate type for the GUI
type ItemTemplate struct {
	quick.QQuickItem

	_ func(string)    `signal:"startTracker,auto"`
	_ func()          `signal:"stopTracker,auto"`
	_ func()          `signal:"updateJIRA,auto"`
	_ *ObjectTemplate `property:"activityLabel"`
	_ *ObjectTemplate `property:"startButton"`
}

func (t *ItemTemplate) startTracker(name string) {
	if name != "" && strings.ToLower(t.ActivityLabel().Text()) != name {

		if t.ActivityLabel().Text() != "" {
			lblText := strings.ToLower(t.ActivityLabel().Text())
			tracker.stop()
			log.Printf("Stopped %s\n", lblText)
			// Don't track any thing shorter than 1 minute
			if int(tracker.duration()) >= 1 {
				a := activity{}
				a.log(lblText, tracker.strStartTime(),
					tracker.strStopTime(), int(tracker.duration()))
				activities = append(activities, a)
				writeActivitiesToFile()
			}
		}
		tracker.start(name)
		t.ActivityLabel().SetText(strings.ToUpper(name))
		if activityTicketName[name] != "" {
			log.Printf("Started %s (%s)\n",
				name, activityTicketName[name])
		} else {
			log.Printf("Started %s\n", name)
		}
	}
}

func (t *ItemTemplate) stopTracker() {
	if t.ActivityLabel().Text() != "" {
		lblText := strings.ToLower(t.ActivityLabel().Text())
		tracker.stop()
		log.Printf("Stopped %s\n", lblText)
		// Don't track any thing shorter than 5 minutes
		if int(tracker.duration()) >= 1 {
			a := activity{}
			a.log(lblText, tracker.strStartTime(),
				tracker.strStopTime(), int(tracker.duration()))
			activities = append(activities, a)
			writeActivitiesToFile()
		}
		t.ActivityLabel().SetText("")
	}
}

func (t *ItemTemplate) updateJIRA() {
	if len(activities) > 0 {
		aggr := aggregateActivityDuration(activities)
		log.Printf("Updating JIRA with %d new worklogs\n", len(aggr))
		for _, act := range aggr {
			log.Printf("Updating %s worklog with %d minutes", act.Ticket, act.Duration)
			addWorkLogToJIRA(act)
		}
		renameTimeTrackerFile()
		activities = []activity{}
	}
}

func aggregateActivityDuration(activities []activity) []activity {

	log.Printf("Aggregating duration from %d activities", len(activities))
	aggregated := []activity{}
	var aggrTickets []string
	for i, act := range activities {

		if isInSlice(aggrTickets, act.Ticket) {
			continue
		}
		bact := act
		for x := i + 1; x < len(activities); x++ {
			if act.Ticket == activities[x].Ticket {
				bact.Duration += activities[x].Duration
			}
		}
		aggregated = append(aggregated, bact)
		aggrTickets = append(aggrTickets, bact.Ticket)
	}

	return aggregated
}

func isInSlice(slice []string, a string) bool {
	for _, v := range slice {
		if a == v {
			return true
		}
	}
	return false
}

//ObjectTemplate type to bridge with qml
type ObjectTemplate struct {
	core.QObject

	_ string `property:"text"`
}

type timeTracker struct {
	activity  string
	startTime time.Time
	stopTime  time.Time
}

func (t *timeTracker) start(activity string) {
	t.activity = activity
	t.startTime = time.Now()
}

func (t *timeTracker) stop() {
	t.stopTime = time.Now()
}

func (t *timeTracker) duration() float64 {
	duration := t.stopTime.Sub(t.startTime)
	//TODO: Change to minutes
	return duration.Minutes()
}

func (t *timeTracker) strStartTime() string {
	//Return time on the format the JIRA API demands
	return t.startTime.Format("2006-01-02T15:04:05.000Z0700")
}

func (t *timeTracker) strStopTime() string {
	//Return time on the format the JIRA API demands
	return t.stopTime.Format("2006-01-02T15:04:05.000Z0700")
}

type activity struct {
	Name      string
	Ticket    string
	StartTime string
	StopTime  string
	Duration  int
}

func (a *activity) log(name, startTime, stopTime string, duration int) {
	a.Name = name
	a.Ticket = activityTicketName[name]
	if a.Ticket == "" {
		a.Ticket = a.Name
	}
	a.StartTime = startTime
	a.StopTime = stopTime
	a.Duration = duration
}

type config struct {
	JiraURL  string
	Username string
	Password string
}

func renameTimeTrackerFile() {
	oldfname := "timetracker_" + time.Now().Format("02.Jan.2006") + ".yaml"
	newfname := "timetracker_" + time.Now().Format("02.Jan.2006_150405") + ".yaml"
	os.Rename(path.Join(ttlogdir, oldfname), path.Join(ttlogdir, newfname))
}

func writeActivitiesToFile() {

	filename := "timetracker_" + time.Now().Format("02.Jan.2006") + ".yaml"
	yamlstr, err := yaml.Marshal(activities)
	if err != nil {
		log.Fatalln(err)
	}
	err = ioutil.WriteFile(path.Join(ttlogdir, filename), yamlstr, 0644)
	if err != nil {
		log.Fatalf("Failed to write file: %v", err)
	}
}

func loadActivitiesFromFile(filename string) {
	yamlAct, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println(err)
	}
	err = yaml.Unmarshal(yamlAct, &activities)
	if err != nil {
		log.Println(err)
	}
	log.Printf("Loaded %d activities from file\n", len(activities))
}

func addWorkLogToJIRA(act activity) {
	log.Printf("Adding worklog to: %s, time spent: %d minutes\n",
		strings.ToUpper(act.Ticket), act.Duration)
	//Create the jira url
	url := cfg.JiraURL + "/rest/api/2/issue/" + strings.ToUpper(act.Ticket) + "/worklog"

	// Create payload
	payload := []byte(`{
	"timeSpent": "` + strconv.Itoa(act.Duration) + `m",
	"started": "` + act.StopTime + `"
    }`)

	// Create request
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.SetBasicAuth(cfg.Username, cfg.Password)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	log.Println("JIRA response status", resp.Status)
}

func loadConfig(home string) {
	cfg = config{}
	configfile := path.Join(home, ".config/time-tracker/config.yaml")
	if _, err := os.Stat(configfile); err == nil {
		cfgYaml, err := ioutil.ReadFile(configfile)
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		yaml.Unmarshal(cfgYaml, &cfg)
	} else {
		log.Fatalf("%s is missing", configfile)
		os.Exit(1)
	}

	if cfg.Password[0:5] == "pass:" {
		cmdargs := strings.Split(cfg.Password, ":")
		output, err := exec.Command(cmdargs[0], cmdargs[1]).CombinedOutput()
		if err != nil {
			log.Fatal(err)
		}
		cfg.Password = strings.TrimSpace(string(output))
	}
}

func main() {

	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	widgets.NewQApplication(len(os.Args), os.Args)

	quickcontrols2.QQuickStyle_SetStyle("Material")

	view := quick.NewQQuickView(nil)
	view.SetMinimumSize(core.NewQSize2(400, 160))
	view.SetResizeMode(quick.QQuickView__SizeRootObjectToView)
	view.SetTitle("Time Tracker")

	view.SetSource(core.NewQUrl3("qrc:/qml/main.qml", 0))
	view.Show()

	widgets.QApplication_Exec()
}
